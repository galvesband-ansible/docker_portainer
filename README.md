Role Name
=========

Launchs a portainer container (with no agent) in docker on Ubuntu 20.04 with SSL.

Requirements
------------

None.

Role Variables
--------------

This role sets up Portainer with an SSL certificate. Other roles in
[this gitlab group](https://gitlab.com/galvesband-ansible) provide the means 
to generate this certificate through a very simple certificate authority 
(not suitable for production or public networks), but you can use any 
certificate as long as ansible is able to access the machine that holds the 
certificate. It can be itself.

 - `portainer_custom_cert`: If `yes`, a certificate will be fetched from 
   a remote host defined in `portainer_cert_host`. Defaults to `no`.

 - `portainer_expose_http`: If `yes`, the non-https port of the portainer
   interface will be exposed. Tipically the port is `9000`. Defaults to `no`.

 - `portainer_expose_edge`: If `yes`, the port dedicated to receive 
   communications of edge agents will be exposed. Tipically the
   port is `8000`. Defaults to `yes`.

 - `portainer_http_port`: Port for HTTP. Defaults to `9000`.

 - `portainer_https_port`: Port for HTTPS. Defaults to `9443`.

 - `portainer_edge_port`: Port for edge agents communication.
   Defaults to `8000`.

 - `portainer_ssl`: If `no`, the non https port `9000` will be exposed
   and no attempt will be made to fetch a certificate.

 - `portainer_cert_src_path`: Path to the public certificate on `portainer_cert_host`. 
   Defaults to `/srv/certs/portainer.local/cert.crt`. Ignored if `portainer_ssl` is `no`.

 - `portainer_key_src_path`: Path to the private key on `portainer_cert_host`. 
   Defaults to `/srv/certs/portainer.local/cert.key`. Ignored if `portainer_ssl` is `no`.

 - `portainer_cert_host`: Inventory name of the host that owns the certificate.
   Defaults to `localhost`. Ignored if `portainer_ssl` is `no`.


Dependencies
------------

None, although the roles in [this gitlab group](https://gitlab.com/galvesband-ansible)
and this role are designed to work together. See the example below.


Example Playbook
----------------

The simplest example is without using a custom certificate.

```yml
- host: my-server
  gather_facts: yes
  roles:

   - name: docker

   # Opens ports 9443 (https, self-signed) without edge agent (8000)
   - name: docker_portainer
     vars:
       portainer_expose_edge: no
```

In this example, other roles like `certificate_authority` and 
`ca_based_certificate` are used to build the certificate for Portainer. 

```yml
- hosts: my-server
  gather_facts: yes
  roles:

    # Build a simple CA in this server
    - name: certificate_authority
      vars:
        ca_name: my-own-ca

    # Build a certificate for portainer in this server also
    - name: ca_based_certificate
      vars:
        ca_based_certificate_ca_name: my-own-ca
        ca_based_certificate_ca_host: my-server
        ca_based_certificate_fqdn: my-portainer.local

    # Installs docker
    - name: docker
      
    # Opens 9443 (with a cert signed by my-own-ca for my-portainer.local fqdn) and 8000
    - name: docker_portainer
      vars:
        portainer_ssl: yes
        portainer_custom_cert: yes
        # Location of public and private key in portainer_cert_host
        portainer_cert_src_path: /srv/certs/my-portainer.local/cert.crt
        portainer_key_src_path: /srv/certs/my-portainer.local/cert.key
        # The server that holds the certificate is also this.
        portainer_cert_host: my-server
```


License
-------

[GPL 2.0 Or later](https://spdx.org/licenses/GPL-2.0-or-later.html).
